# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'login.ui'
##
## Created by: Qt User Interface Compiler version 5.14.0
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from qtpy.QtCore import (QCoreApplication, QMetaObject, QObject, QPoint,
    QRect, QSize, QUrl, Qt)
from qtpy.QtGui import (QBrush, QColor, QConicalGradient, QFont,
    QFontDatabase, QIcon, QLinearGradient, QPalette, QPainter, QPixmap,
    QRadialGradient , QCursor)
from qtpy.QtWidgets import *

class Ui_LoginDialog(object):
    def setupUi(self, LoginDialog):
        if LoginDialog.objectName():
            LoginDialog.setObjectName(u"LoginDialog")
        LoginDialog.resize(898, 532)
        LoginDialog.setStyleSheet(u"background-color:#FFFFFF;\n"
"")
        self.horizontalLayout = QHBoxLayout(LoginDialog)
        self.horizontalLayout.setSpacing(0)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setSizeConstraint(QLayout.SetDefaultConstraint)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_2 = QVBoxLayout()
        self.verticalLayout_2.setSpacing(0)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.label_2 = QLabel(LoginDialog)
        self.label_2.setObjectName(u"label_2")
        self.label_2.setMinimumSize(QSize(430, 530))
        self.label_2.setMaximumSize(QSize(430, 530))
        self.label_2.setFrameShape(QFrame.HLine)
        self.label_2.setPixmap(QPixmap(u":/Images/Images/about5.jpg"))
        self.label_2.setScaledContents(True)
        self.label_2.setAlignment(Qt.AlignCenter)
        self.label_2.setWordWrap(True)

        self.verticalLayout_2.addWidget(self.label_2)


        self.horizontalLayout.addLayout(self.verticalLayout_2)

        self.verticalLayout = QVBoxLayout()
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setSizeConstraint(QLayout.SetMaximumSize)
        self.verticalLayout.setContentsMargins(50, -1, 50, -1)
        self.label_3 = QLabel(LoginDialog)
        self.label_3.setObjectName(u"label_3")
        self.label_3.setMaximumSize(QSize(16777215, 70))
        font = QFont()
        font.setFamily(u"Bahnschrift SemiBold SemiConden")
        font.setPointSize(14)
        font.setBold(False)
        font.setWeight(50)
        self.label_3.setFont(font)
        self.label_3.setStyleSheet(u"color:#1FCC44;")
        self.label_3.setWordWrap(True)

        self.verticalLayout.addWidget(self.label_3)

        self.verticalSpacer_2 = QSpacerItem(10, 10, QSizePolicy.Minimum, QSizePolicy.Fixed)

        self.verticalLayout.addItem(self.verticalSpacer_2)

        self.label = QLabel(LoginDialog)
        self.label.setObjectName(u"label")
        self.label.setMaximumSize(QSize(16777215, 50))
        font1 = QFont()
        font1.setPointSize(15)
        font1.setBold(True)
        font1.setWeight(75)
        self.label.setFont(font1)
        self.label.setStyleSheet(u"color:#4b4b5a;\n"
"font-size:17.5px;\n"
"")
        self.label.setScaledContents(True)
        self.label.setAlignment(Qt.AlignLeading|Qt.AlignLeft|Qt.AlignVCenter)

        self.verticalLayout.addWidget(self.label)

        self.label_6 = QLabel(LoginDialog)
        self.label_6.setObjectName(u"label_6")
        self.label_6.setMaximumSize(QSize(16777215, 30))
        self.label_6.setStyleSheet(u"color:#6c757d;\n"
"padding:0;\n"
"margin:0;")
        self.label_6.setTextFormat(Qt.AutoText)
        self.label_6.setScaledContents(False)
        self.label_6.setWordWrap(True)

        self.verticalLayout.addWidget(self.label_6)

        self.verticalSpacer = QSpacerItem(20, 10, QSizePolicy.Minimum, QSizePolicy.Fixed)

        self.verticalLayout.addItem(self.verticalSpacer)

        self.lineEdit = QLineEdit(LoginDialog)
        self.lineEdit.setObjectName(u"lineEdit")
        self.lineEdit.setMinimumSize(QSize(0, 65))
        self.lineEdit.setStyleSheet(u"background-color: #E5E5E5;\n"
"width: 90%;\n"
"border: none;\n"
"font-size: 1.3rem;\n"
"font-weight: 400;\n"
"padding-left: 30px;\n"
"color:#6b6b6b;\n"
"        height: 60px;\n"
"        /* make the borders more round */\n"
"        border-radius: 30px;\n"
"")

        self.verticalLayout.addWidget(self.lineEdit)

        self.label_4 = QLabel(LoginDialog)
        self.label_4.setObjectName(u"label_4")
        self.label_4.setMaximumSize(QSize(16777215, 20))
        font2 = QFont()
        font2.setBold(True)
        font2.setItalic(True)
        font2.setWeight(75)
        self.label_4.setFont(font2)
        self.label_4.setStyleSheet(u"color:#6c757d;\n"
"")

        self.verticalLayout.addWidget(self.label_4)

        self.lineEdit_2 = QLineEdit(LoginDialog)
        self.lineEdit_2.setObjectName(u"lineEdit_2")
        self.lineEdit_2.setMaximumSize(QSize(16777215, 65))
        self.lineEdit_2.setStyleSheet(u"background-color: #E5E5E5;\n"
"width: 90%;\n"
"border: none;\n"
"font-size: 1.3rem;\n"
"font-weight: 400;\n"
"padding-left: 30px;\n"
"color:#6b6b6b;\n"
"        height: 60px;\n"
"        /* make the borders more round */\n"
"        border-radius: 30px;\n"
"")
        self.lineEdit_2.setEchoMode(QLineEdit.Password)

        self.verticalLayout.addWidget(self.lineEdit_2)

        self.label_5 = QLabel(LoginDialog)
        self.label_5.setObjectName(u"label_5")
        self.label_5.setMaximumSize(QSize(16777215, 20))
        self.label_5.setFont(font2)
        self.label_5.setStyleSheet(u"color:#6c757d;\n"
"")

        self.verticalLayout.addWidget(self.label_5)

        self.pushButton = QPushButton(LoginDialog)
        self.pushButton.setObjectName(u"pushButton")
        self.pushButton.setCursor(QCursor(Qt.PointingHandCursor))
        self.pushButton.setStyleSheet(u"        height: 60px;\n"
"        /* make the borders more round */\n"
"        border-radius: 30px;\n"
"        width: 100%;\n"
"        background-color: #1FCC44;\n"
"        padding-left: 0;\n"
"        font-weight: bold;\n"
"        color: white;\n"
"        text-transform: uppercase;\n"
"")

        self.verticalLayout.addWidget(self.pushButton)


        self.horizontalLayout.addLayout(self.verticalLayout)


        self.retranslateUi(LoginDialog)

        QMetaObject.connectSlotsByName(LoginDialog)
    # setupUi

    def retranslateUi(self, LoginDialog):
        LoginDialog.setWindowTitle(QCoreApplication.translate("LoginDialog", u"Dialog", None))
        self.label_2.setText("")
        self.label_3.setText(QCoreApplication.translate("LoginDialog", u"Bienvenue dans ROBOT MANIPULATEUR 2D", None))
        self.label.setText(QCoreApplication.translate("LoginDialog", u"Se Connecter !", None))
        self.label_6.setText(QCoreApplication.translate("LoginDialog", u"Entrez votre email et mot de passe pour acc\u00e9der \u00e0 l'interface d'administration. ", None))
        self.lineEdit.setPlaceholderText(QCoreApplication.translate("LoginDialog", u"Nom d'utilisateur", None))
        self.label_4.setText(QCoreApplication.translate("LoginDialog", u"Mot de passe par d\u00e9faut : root", None))
        self.lineEdit_2.setText("")
        self.lineEdit_2.setPlaceholderText(QCoreApplication.translate("LoginDialog", u"Mot de Passe", None))
        self.label_5.setText(QCoreApplication.translate("LoginDialog", u"Mot de passe par d\u00e9faut : root", None))
        self.pushButton.setText(QCoreApplication.translate("LoginDialog", u"Se Connecter", None))
    # retranslateUi

