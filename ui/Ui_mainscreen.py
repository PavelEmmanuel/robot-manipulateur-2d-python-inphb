# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'mainscreen.ui'
##
## Created by: Qt User Interface Compiler version 5.14.0
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from qtpy.QtCore import (QCoreApplication, QMetaObject, QObject, QPoint,
    QRect, QSize, QUrl, Qt)
from qtpy.QtGui import (QBrush, QColor, QConicalGradient, QFont,
    QFontDatabase, QIcon, QLinearGradient, QPalette, QPainter, QPixmap,
    QRadialGradient , QCursor)
from qtpy.QtWidgets import QMenu , QGridLayout , QMenuBar , QPushButton , QLabel , QStatusBar , QSizePolicy , QSpacerItem , QVBoxLayout , QHBoxLayout , QFrame , QSpinBox , QDoubleSpinBox , QGroupBox , QWidget

from mplwidget import MplWidget
class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(1261, 708)
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.horizontalLayout_3 = QHBoxLayout(self.centralwidget)
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.verticalLayout = QVBoxLayout()
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.groupBox_5 = QGroupBox(self.centralwidget)
        self.groupBox_5.setObjectName(u"groupBox_5")
        self.verticalLayout_2 = QVBoxLayout(self.groupBox_5)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.label_16 = QLabel(self.groupBox_5)
        self.label_16.setObjectName(u"label_16")
        font = QFont()
        font.setPointSize(11)
        font.setBold(True)
        font.setWeight(75);
        self.label_16.setFont(font)
        self.label_16.setAlignment(Qt.AlignCenter)

        self.verticalLayout_2.addWidget(self.label_16)

        self.gridLayout_2 = QGridLayout()
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.label_17 = QLabel(self.groupBox_5)
        self.label_17.setObjectName(u"label_17")

        self.horizontalLayout.addWidget(self.label_17)

        self.label_18 = QLabel(self.groupBox_5)
        self.label_18.setObjectName(u"label_18")

        self.horizontalLayout.addWidget(self.label_18)


        self.gridLayout_2.addLayout(self.horizontalLayout, 0, 0, 1, 1)

        self.horizontalLayout_4 = QHBoxLayout()
        self.horizontalLayout_4.setObjectName(u"horizontalLayout_4")
        self.label_21 = QLabel(self.groupBox_5)
        self.label_21.setObjectName(u"label_21")

        self.horizontalLayout_4.addWidget(self.label_21)

        self.label_22 = QLabel(self.groupBox_5)
        self.label_22.setObjectName(u"label_22")

        self.horizontalLayout_4.addWidget(self.label_22)


        self.gridLayout_2.addLayout(self.horizontalLayout_4, 0, 1, 1, 1)

        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.label_19 = QLabel(self.groupBox_5)
        self.label_19.setObjectName(u"label_19")

        self.horizontalLayout_2.addWidget(self.label_19)

        self.label_20 = QLabel(self.groupBox_5)
        self.label_20.setObjectName(u"label_20")

        self.horizontalLayout_2.addWidget(self.label_20)


        self.gridLayout_2.addLayout(self.horizontalLayout_2, 1, 0, 1, 1)

        self.horizontalLayout_11 = QHBoxLayout()
        self.horizontalLayout_11.setObjectName(u"horizontalLayout_11")
        self.label_23 = QLabel(self.groupBox_5)
        self.label_23.setObjectName(u"label_23")

        self.horizontalLayout_11.addWidget(self.label_23)

        self.label_24 = QLabel(self.groupBox_5)
        self.label_24.setObjectName(u"label_24")

        self.horizontalLayout_11.addWidget(self.label_24)


        self.gridLayout_2.addLayout(self.horizontalLayout_11, 1, 1, 1, 1)


        self.verticalLayout_2.addLayout(self.gridLayout_2)


        self.verticalLayout.addWidget(self.groupBox_5)

        self.groupBox = QGroupBox(self.centralwidget)
        self.groupBox.setObjectName(u"groupBox")
        self.verticalLayout_3 = QVBoxLayout(self.groupBox)
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.label_2 = QLabel(self.groupBox)
        self.label_2.setObjectName(u"label_2")
        self.label_2.setFont(font)
        self.label_2.setAlignment(Qt.AlignCenter)

        self.verticalLayout_3.addWidget(self.label_2)

        self.horizontalLayout_5 = QHBoxLayout()
        self.horizontalLayout_5.setObjectName(u"horizontalLayout_5")
        self.label_3 = QLabel(self.groupBox)
        self.label_3.setObjectName(u"label_3")

        self.horizontalLayout_5.addWidget(self.label_3)

        self.doubleSpinBox_3 = QDoubleSpinBox(self.groupBox)
        self.doubleSpinBox_3.setObjectName(u"doubleSpinBox_3")

        self.horizontalLayout_5.addWidget(self.doubleSpinBox_3)


        self.verticalLayout_3.addLayout(self.horizontalLayout_5)

        self.horizontalLayout_6 = QHBoxLayout()
        self.horizontalLayout_6.setObjectName(u"horizontalLayout_6")
        self.label_4 = QLabel(self.groupBox)
        self.label_4.setObjectName(u"label_4")

        self.horizontalLayout_6.addWidget(self.label_4)

        self.doubleSpinBox_2 = QDoubleSpinBox(self.groupBox)
        self.doubleSpinBox_2.setObjectName(u"doubleSpinBox_2")

        self.horizontalLayout_6.addWidget(self.doubleSpinBox_2)


        self.verticalLayout_3.addLayout(self.horizontalLayout_6)

        self.horizontalLayout_7 = QHBoxLayout()
        self.horizontalLayout_7.setObjectName(u"horizontalLayout_7")
        self.label_5 = QLabel(self.groupBox)
        self.label_5.setObjectName(u"label_5")

        self.horizontalLayout_7.addWidget(self.label_5)

        self.doubleSpinBox = QDoubleSpinBox(self.groupBox)
        self.doubleSpinBox.setObjectName(u"doubleSpinBox")

        self.horizontalLayout_7.addWidget(self.doubleSpinBox)


        self.verticalLayout_3.addLayout(self.horizontalLayout_7)


        self.verticalLayout.addWidget(self.groupBox)

        self.groupBox_2 = QGroupBox(self.centralwidget)
        self.groupBox_2.setObjectName(u"groupBox_2")
        self.verticalLayout_5 = QVBoxLayout(self.groupBox_2)
        self.verticalLayout_5.setObjectName(u"verticalLayout_5")
        self.label_6 = QLabel(self.groupBox_2)
        self.label_6.setObjectName(u"label_6")
        self.label_6.setFont(font)
        self.label_6.setAlignment(Qt.AlignCenter)

        self.verticalLayout_5.addWidget(self.label_6)

        self.horizontalLayout_9 = QHBoxLayout()
        self.horizontalLayout_9.setObjectName(u"horizontalLayout_9")
        self.label_7 = QLabel(self.groupBox_2)
        self.label_7.setObjectName(u"label_7")

        self.horizontalLayout_9.addWidget(self.label_7)

        self.spinBox = QSpinBox(self.groupBox_2)
        self.spinBox.setObjectName(u"spinBox")
        self.spinBox.setMaximum(360)

        self.horizontalLayout_9.addWidget(self.spinBox)


        self.verticalLayout_5.addLayout(self.horizontalLayout_9)

        self.horizontalLayout_8 = QHBoxLayout()
        self.horizontalLayout_8.setObjectName(u"horizontalLayout_8")
        self.label_8 = QLabel(self.groupBox_2)
        self.label_8.setObjectName(u"label_8")

        self.horizontalLayout_8.addWidget(self.label_8)

        self.spinBox_2 = QSpinBox(self.groupBox_2)
        self.spinBox_2.setObjectName(u"spinBox_2")
        self.spinBox_2.setMaximum(360)

        self.horizontalLayout_8.addWidget(self.spinBox_2)


        self.verticalLayout_5.addLayout(self.horizontalLayout_8)


        self.verticalLayout.addWidget(self.groupBox_2)

        self.groupBox_3 = QGroupBox(self.centralwidget)
        self.groupBox_3.setObjectName(u"groupBox_3")
        self.verticalLayout_4 = QVBoxLayout(self.groupBox_3)
        self.verticalLayout_4.setObjectName(u"verticalLayout_4")
        self.label = QLabel(self.groupBox_3)
        self.label.setObjectName(u"label")
        self.label.setFont(font)
        self.label.setAlignment(Qt.AlignCenter)

        self.verticalLayout_4.addWidget(self.label)

        self.gridLayout = QGridLayout()
        self.gridLayout.setObjectName(u"gridLayout")
        self.label_9 = QLabel(self.groupBox_3)
        self.label_9.setObjectName(u"label_9")

        self.gridLayout.addWidget(self.label_9, 0, 0, 1, 1)

        self.doubleSpinBox_4 = QDoubleSpinBox(self.groupBox_3)
        self.doubleSpinBox_4.setObjectName(u"doubleSpinBox_4")

        self.gridLayout.addWidget(self.doubleSpinBox_4, 0, 1, 1, 1)

        self.label_10 = QLabel(self.groupBox_3)
        self.label_10.setObjectName(u"label_10")

        self.gridLayout.addWidget(self.label_10, 1, 0, 1, 1)

        self.doubleSpinBox_5 = QDoubleSpinBox(self.groupBox_3)
        self.doubleSpinBox_5.setObjectName(u"doubleSpinBox_5")

        self.gridLayout.addWidget(self.doubleSpinBox_5, 1, 1, 1, 1)


        self.verticalLayout_4.addLayout(self.gridLayout)


        self.verticalLayout.addWidget(self.groupBox_3)

        self.groupBox_4 = QGroupBox(self.centralwidget)
        self.groupBox_4.setObjectName(u"groupBox_4")
        self.verticalLayout_6 = QVBoxLayout(self.groupBox_4)
        self.verticalLayout_6.setObjectName(u"verticalLayout_6")
        self.label_11 = QLabel(self.groupBox_4)
        self.label_11.setObjectName(u"label_11")
        self.label_11.setFont(font)
        self.label_11.setAlignment(Qt.AlignCenter)

        self.verticalLayout_6.addWidget(self.label_11)

        self.horizontalLayout_10 = QHBoxLayout()
        self.horizontalLayout_10.setObjectName(u"horizontalLayout_10")
        self.label_12 = QLabel(self.groupBox_4)
        self.label_12.setObjectName(u"label_12")

        self.horizontalLayout_10.addWidget(self.label_12)

        self.spinBox_3 = QSpinBox(self.groupBox_4)
        self.spinBox_3.setObjectName(u"spinBox_3")
        self.spinBox_3.setMinimum(1)
        self.spinBox_3.setMaximum(1000)

        self.horizontalLayout_10.addWidget(self.spinBox_3)


        self.verticalLayout_6.addLayout(self.horizontalLayout_10)


        self.verticalLayout.addWidget(self.groupBox_4)


        self.horizontalLayout_3.addLayout(self.verticalLayout)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_3.addItem(self.horizontalSpacer)

        self.verticalLayout_9 = QVBoxLayout()
        self.verticalLayout_9.setObjectName(u"verticalLayout_9")
        self.label_statut = QLabel(self.centralwidget)
        self.label_statut.setObjectName(u"label_statut")
        self.label_statut.setMaximumSize(QSize(16777215, 20))
        font1 = QFont()
        font1.setPointSize(15)
        font1.setBold(True)
        font1.setWeight(75);
        self.label_statut.setFont(font1)
        self.label_statut.setStyleSheet(u"color:red;")
        self.label_statut.setAlignment(Qt.AlignCenter)

        self.verticalLayout_9.addWidget(self.label_statut)

        self.MplWidget = MplWidget(self.centralwidget)
        self.MplWidget.setObjectName(u"MplWidget")
        self.MplWidget.setMinimumSize(QSize(700, 0))

        self.verticalLayout_9.addWidget(self.MplWidget)


        self.horizontalLayout_3.addLayout(self.verticalLayout_9)

        self.sidebarFrame = QFrame(self.centralwidget)
        self.sidebarFrame.setObjectName(u"sidebarFrame")
        self.sidebarFrame.setMinimumSize(QSize(250, 0))
        self.sidebarFrame.setMaximumSize(QSize(250, 16777215))
        self.sidebarFrame.setStyleSheet(u"background-color:#ffffff;\n"
"")
        self.sidebarFrame.setFrameShape(QFrame.StyledPanel)
        self.sidebarFrame.setFrameShadow(QFrame.Raised)
        self.verticalLayout_7 = QVBoxLayout(self.sidebarFrame)
        self.verticalLayout_7.setSpacing(0)
        self.verticalLayout_7.setObjectName(u"verticalLayout_7")
        self.verticalLayout_7.setContentsMargins(0, 0, 0, 0)
        self.label_13 = QLabel(self.sidebarFrame)
        self.label_13.setObjectName(u"label_13")
        self.label_13.setMinimumSize(QSize(0, 80))
        font2 = QFont()
        font2.setBold(False)
        font2.setWeight(50);
        self.label_13.setFont(font2)
        self.label_13.setStyleSheet(u"background-color:#fff;color:#4b4b5a;\n"
"font-weight:400;\n"
"text-align:left;\n"
"border:none;\n"
"color: #5369f8;\n"
"background-color: #f7f7ff;\n"
"\n"
"")
        self.label_13.setAlignment(Qt.AlignCenter)

        self.verticalLayout_7.addWidget(self.label_13)

        self.frame_6 = QFrame(self.sidebarFrame)
        self.frame_6.setObjectName(u"frame_6")
        self.frame_6.setStyleSheet(u"QFrame::item{\n"
"border-left: 3px solid #5369f8;\n"
"color: #5369f8;\n"
"background-color: #f7f7ff;\n"
"}")
        self.frame_6.setFrameShape(QFrame.StyledPanel)
        self.frame_6.setFrameShadow(QFrame.Raised)
        self.verticalLayout_8 = QVBoxLayout(self.frame_6)
        self.verticalLayout_8.setObjectName(u"verticalLayout_8")
        self.verticalLayout_8.setContentsMargins(0, -1, 0, -1)
        self.label_14 = QLabel(self.frame_6)
        self.label_14.setObjectName(u"label_14")
        self.label_14.setFont(font2)
        self.label_14.setStyleSheet(u"padding:15px 30px 10px 30px;\n"
"font-size:.6875rem;\n"
"color:#4b4b5a;\n"
"font-weight:400;")
        self.label_14.setScaledContents(False)

        self.verticalLayout_8.addWidget(self.label_14)

        self.pushButton_dessiner = QPushButton(self.frame_6)
        self.pushButton_dessiner.setObjectName(u"pushButton_dessiner")
        self.pushButton_dessiner.setMinimumSize(QSize(0, 60))
        self.pushButton_dessiner.setMaximumSize(QSize(16777215, 60))
        self.pushButton_dessiner.setFont(font2)
        self.pushButton_dessiner.setCursor(QCursor(Qt.PointingHandCursor))
        self.pushButton_dessiner.setStyleSheet(u"color:#ffffff;\n"
"font-weight:400;\n"
"font-size:18px;\n"
"text-align:left;\n"
"padding-left:30px;\n"
"border:none;\n"
"border-left: 3px solid #5369f8;\n"
"background-color:  #8a948a;\n"
"\n"
"")
        icon = QIcon()
        icon.addFile(u":/Images/Images/pencil.png", QSize(), QIcon.Normal, QIcon.Off)
        self.pushButton_dessiner.setIcon(icon)
        self.pushButton_dessiner.setFlat(False)

        self.verticalLayout_8.addWidget(self.pushButton_dessiner)

        self.pushButton_mouvement = QPushButton(self.frame_6)
        self.pushButton_mouvement.setObjectName(u"pushButton_mouvement")
        self.pushButton_mouvement.setMinimumSize(QSize(0, 60))
        self.pushButton_mouvement.setMaximumSize(QSize(16777215, 60))
        self.pushButton_mouvement.setCursor(QCursor(Qt.PointingHandCursor))
        self.pushButton_mouvement.setStyleSheet(u"color:#ffffff;\n"
"font-weight:400;\n"
"font-size:18px;\n"
"text-align:left;\n"
"padding-left:30px;\n"
"border:none;\n"
"border-left: 3px solid #5369f8;\n"
"background-color:  #42f54e;\n"
"")
        icon1 = QIcon()
        icon1.addFile(u":/Images/Images/play.png", QSize(), QIcon.Normal, QIcon.Off)
        self.pushButton_mouvement.setIcon(icon1)
        self.pushButton_mouvement.setFlat(False)

        self.verticalLayout_8.addWidget(self.pushButton_mouvement)

        self.label_15 = QLabel(self.frame_6)
        self.label_15.setObjectName(u"label_15")
        self.label_15.setFont(font2)
        self.label_15.setStyleSheet(u"padding:15px 30px 10px 30px;\n"
"font-size:.6875rem;\n"
"color:#4b4b5a;\n"
"font-weight:400;")

        self.verticalLayout_8.addWidget(self.label_15)

        self.pushButton_reprendre = QPushButton(self.frame_6)
        self.pushButton_reprendre.setObjectName(u"pushButton_reprendre")
        self.pushButton_reprendre.setMinimumSize(QSize(0, 60))
        self.pushButton_reprendre.setMaximumSize(QSize(16777215, 60))
        self.pushButton_reprendre.setCursor(QCursor(Qt.PointingHandCursor))
        self.pushButton_reprendre.setStyleSheet(u"color:#ffffff;\n"
"font-weight:400;\n"
"font-size:18px;\n"
"text-align:left;\n"
"padding-left:30px;\n"
"border:none;\n"
"border-left: 3px solid #5369f8;\n"
"background-color:  #42f54e;\n"
"\n"
"")
        icon2 = QIcon()
        icon2.addFile(u":/Images/Images/reload.png", QSize(), QIcon.Normal, QIcon.Off)
        self.pushButton_reprendre.setIcon(icon2)
        self.pushButton_reprendre.setFlat(False)

        self.verticalLayout_8.addWidget(self.pushButton_reprendre)

        self.pushButton_quitter = QPushButton(self.frame_6)
        self.pushButton_quitter.setObjectName(u"pushButton_quitter")
        self.pushButton_quitter.setMinimumSize(QSize(0, 60))
        self.pushButton_quitter.setStyleSheet(u"color:#ffffff;\n"
"font-weight:400;\n"
"font-size:18px;\n"
"text-align:left;\n"
"padding-left:30px;\n"
"border:none;\n"
"border-left: 3px solid #5369f8;\n"
"background-color:  #d93125;\n"
"")

        self.verticalLayout_8.addWidget(self.pushButton_quitter)


        self.verticalLayout_7.addWidget(self.frame_6)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_7.addItem(self.verticalSpacer)


        self.horizontalLayout_3.addWidget(self.sidebarFrame)

        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QMenuBar(MainWindow)
        self.menubar.setObjectName(u"menubar")
        self.menubar.setGeometry(QRect(0, 0, 1261, 26))
        self.menuFichier = QMenu(self.menubar)
        self.menuFichier.setObjectName(u"menuFichier")
        self.menuAide = QMenu(self.menubar)
        self.menuAide.setObjectName(u"menuAide")
        self.menuA_propos = QMenu(self.menubar)
        self.menuA_propos.setObjectName(u"menuA_propos")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QStatusBar(MainWindow)
        self.statusbar.setObjectName(u"statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.menubar.addAction(self.menuFichier.menuAction())
        self.menubar.addAction(self.menuAide.menuAction())
        self.menubar.addAction(self.menuA_propos.menuAction())

        self.retranslateUi(MainWindow)

        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"MainWindow", None))
        self.groupBox_5.setTitle(QCoreApplication.translate("MainWindow", u"Donn\u00e9es", None))
        self.label_16.setText(QCoreApplication.translate("MainWindow", u"Position actuelle du robot", None))
        self.label_17.setText(QCoreApplication.translate("MainWindow", u"X:", None))
        self.label_18.setText(QCoreApplication.translate("MainWindow", u"0", None))
        self.label_21.setText(QCoreApplication.translate("MainWindow", u"\u019f1:", None))
        self.label_22.setText(QCoreApplication.translate("MainWindow", u"0", None))
        self.label_19.setText(QCoreApplication.translate("MainWindow", u"Y:", None))
        self.label_20.setText(QCoreApplication.translate("MainWindow", u"0", None))
        self.label_23.setText(QCoreApplication.translate("MainWindow", u"\u019f2:", None))
        self.label_24.setText(QCoreApplication.translate("MainWindow", u"0", None))
        self.groupBox.setTitle(QCoreApplication.translate("MainWindow", u"Valeur des liens", None))
        self.label_2.setText(QCoreApplication.translate("MainWindow", u"Valeurs des liens", None))
        self.label_3.setText(QCoreApplication.translate("MainWindow", u"L0", None))
        self.label_4.setText(QCoreApplication.translate("MainWindow", u"L1", None))
        self.label_5.setText(QCoreApplication.translate("MainWindow", u"L2", None))
        self.groupBox_2.setTitle(QCoreApplication.translate("MainWindow", u"Variables articulaires de d\u00e9part", None))
        self.label_6.setText(QCoreApplication.translate("MainWindow", u"Valeurs de \u019f1 et \u019f2 ", None))
        self.label_7.setText(QCoreApplication.translate("MainWindow", u"\u019f1", None))
        self.label_8.setText(QCoreApplication.translate("MainWindow", u"\u019f2", None))
        self.groupBox_3.setTitle(QCoreApplication.translate("MainWindow", u"Coordonn\u00e9es du point B", None))
        self.label.setText(QCoreApplication.translate("MainWindow", u"Coordonn\u00e9es de B(x0 , Y0)", None))
        self.label_9.setText(QCoreApplication.translate("MainWindow", u"Xb", None))
        self.label_10.setText(QCoreApplication.translate("MainWindow", u"Yb", None))
        self.groupBox_4.setTitle(QCoreApplication.translate("MainWindow", u"Nombre de pas", None))
        self.label_11.setText(QCoreApplication.translate("MainWindow", u"Nombre de pas", None))
        self.label_12.setText(QCoreApplication.translate("MainWindow", u"Nb de pas", None))
        self.label_statut.setText("")
        self.label_13.setText(QCoreApplication.translate("MainWindow", u"ROBOT MANIPULATEUR 2D", None))
        self.label_14.setText(QCoreApplication.translate("MainWindow", u"ACTIONS", None))
        self.pushButton_dessiner.setText(QCoreApplication.translate("MainWindow", u"DESSINER", None))
        self.pushButton_mouvement.setText(QCoreApplication.translate("MainWindow", u"MOUVEMENT", None))
        self.label_15.setText(QCoreApplication.translate("MainWindow", u"GESTION", None))
        self.pushButton_reprendre.setText(QCoreApplication.translate("MainWindow", u"REPRENDRE", None))
        self.pushButton_quitter.setText(QCoreApplication.translate("MainWindow", u"QUITTER", None))
        self.menuFichier.setTitle(QCoreApplication.translate("MainWindow", u"Fichier", None))
        self.menuAide.setTitle(QCoreApplication.translate("MainWindow", u"Aide", None))
        self.menuA_propos.setTitle(QCoreApplication.translate("MainWindow", u"A propos", None))
    # retranslateUi

