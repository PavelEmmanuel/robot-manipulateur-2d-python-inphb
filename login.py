import sys
from qtpy.QtWidgets import  QDialog , QWidget , QTableWidgetItem
# from qtpy.QtCore import QFile
from qtpy import QtGui  
from qtpy.QtGui import QStandardItem , QStandardItemModel

from ui.Ui_login import Ui_LoginDialog
from mainwindow import *

import resource_rc
import qtawesome as qta

import os
os.environ["QT_API"] = "qtpy"


class LoginWindow(QDialog):
    def __init__(self ):
        super(LoginWindow, self).__init__()
        self.ui = Ui_LoginDialog()
        self.ui.setupUi(self)
        self.setWindowTitle("ROBOT MANIPULATEUR 2D")
        self.ui.pushButton.setIcon(qta.icon('mdi.login' , color="white"))


    def on_pushButton_clicked(self):
        if (self.ui.lineEdit.text().lower() == "" and self.ui.lineEdit_2.text().lower() ==""):
            self.ui.label.setText("Connexion réussi")
            self.mainWindow = MainWindow()
            self.mainWindow.show()
            self.hide()
        else:
            self.ui.label.setText("Mot de passe ou nom d'utilisateur incorrecte")


if __name__.endswith('__main__'):
    app = QApplication(sys.argv)
    app.setWindowIcon(QtGui.QIcon(":/Images/Images/robotic_arm.jpg"))
    window = LoginWindow()
    window.show()

    sys.exit(app.exec_())   