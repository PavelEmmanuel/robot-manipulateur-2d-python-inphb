from qtpy.QtWidgets import QApplication, QMainWindow , QWidget , QTableWidgetItem , QHBoxLayout , QMessageBox , QHeaderView
from qtpy.QtCore import QFile
from qtpy import QtGui , QtCore
from qtpy.QtGui import QStandardItem , QStandardItemModel , QCursor

from ui.Ui_mainscreen import Ui_MainWindow
from ui.Ui_help import Ui_Form
import numpy as np
import math
import sys

from matplotlib.animation import FuncAnimation

class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow , self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.setWindowTitle("ROBOT MANIPULATEUR 2D")
        self.ui.pushButton_dessiner.clicked.connect(self.dessiner)
        self.ui.pushButton_mouvement.clicked.connect(self.mouvement)
        self.ui.pushButton_reprendre.clicked.connect(self.reprendre)
        self.ui.pushButton_quitter.clicked.connect(self.quitter)
        self.ui.menuA_propos.triggered.connect(self.a_propos)
        self.ui.menuAide.triggered.connect(self.help)
        # self.ui.pushButton_mouvement.setEnabled(False)
        # self.ui.pushButton_mouvement.setCursor(QCursor(Forni))

        self.canvas = self.ui.MplWidget.canvas
        self.axes = self.ui.MplWidget.canvas.axes

        # self.ui.MplWidget.canvas.axes.style.use('ggplot')
        self.axes.invert_xaxis()
        self.axes.set_title('Robot manipulateur 2D')
        self.axes.set_xlabel('L\'axe des Y')
        self.axes.set_ylabel('L\'axe des X')

        self.axes.yaxis.tick_right()


        self.equation = None
        self.T20 = None
        self.L0 = self.ui.doubleSpinBox_3
        self.L1 = self.ui.doubleSpinBox_2
        self.L2 = self.ui.doubleSpinBox
        self.teta1 = self.ui.spinBox
        self.teta2 = self.ui.spinBox_2
        self.Xb = self.ui.doubleSpinBox_4
        self.Yb = self.ui.doubleSpinBox_5
        self.NbPas = self.ui.spinBox_3
        self.A0 = None
        self.Art20=None
        self.line = None

        self.L0.setValue(3.5)
        self.L1.setValue(3.0)
        self.L2.setValue(3.0)

        self.teta1.setValue(55)
        self.teta2.setValue(75)
        self.Xb.setValue(0)
        self.Yb.setValue(1)
        self.NbPas.setValue(10)
        self.NbPas.setMinimum(1)


    def update_annot(self , ind , i):
        pos = self.linePoint[i].get_offsets()[ind["ind"][0]]
        # self.ui.label_statut.setText(f"P{i+1}({round(pos[1] , 2) } , {round(pos[0] , 2)})")
        self.ui.statusbar.showMessage(f"P{i+1}({round(pos[1] , 2) } , {round(pos[0] , 2)})")
        self.annot.xy = pos
        text = "P{}({:.2g},{:.2g})".format( i+1 , pos[1],pos[0] )
        self.annot.set_text(text)
        self.annot.get_bbox_patch().set_alpha(0.4)


    def hover(self , event):
        vis = self.annot.get_visible()
        visible = False
        if event.inaxes == self.axes:
            for i in range (0 , self.NbPas.value()):
                cont, ind = self.linePoint[i].contains(event)
                if cont:
                    break
            if cont:
                visible = True
                self.update_annot(ind , i)
                self.annot.set_visible(True)
                self.canvas.draw_idle()
            else:
                if vis:
                    self.annot.set_visible(False)
                    self.canvas.draw_idle()
                if visible:
                    self.ui.statusbar.clearMessage()
                    self.ui.label_statut.clear()

    def get_init(self):
        """Obtenir la position initiale dans le repère 0"""
        rteta1 = math.radians(self.teta1.value())
        rteta2 = math.radians(self.teta2.value())

        #matrice de passage de 1 à 0
        T10 = np.matrix([
            [ math.cos(rteta1) , -math.sin(rteta1) , 0 , self.L0.value()],
            [ math.sin(rteta1) , math.cos(rteta1) , 0 , 0],
            [0 , 0, 1 ,0],
            [0 , 0 ,0 ,1]
        ])
        #matrice de passage de 2 à 1
        T21 = np.matrix([
            [math.cos(rteta2) , -math.sin(rteta2) , 0, self.L1.value()],
            [math.sin(rteta2) , math.cos(rteta2) , 0 , 0],
            [0, 0, 1, 0],
            [0, 0, 0, 1]
        ])
        #cordonnes de A dans R2 => A2
        A2= np.matrix([ 
            [self.L2.value()], 
            [0] , 
            [0] , 
            [1]
        ])
        self.T20 = np.dot(T10 , T21)
        #cordonnes de A dans R0 => A0
        self.A0 = np.dot(self.T20 , A2)
        self.A0 = np.squeeze(np.asarray(self.A0))
        #détermination des coordonnes de l'articulation A2 dans R0
        Art22 = np.matrix([
            [0],
            [0],
            [0],
            [1]
        ])
        #Articulation 2 Art2 dans le repère R0
        self.Art20 = np.dot(self.T20 , Art22)
        self.Art20 = np.squeeze(np.asarray(self.Art20))

    def dessiner(self):
        self.ui.label_statut.clear()
        self.get_init()
        #équation de la trajectoire
        self.equation = self.getEquation()
        self.ui.pushButton_mouvement.setEnabled(True)
        if not self.get_portee()[0] :
            self.nbPasARealiser = self.get_portee()[1]
            porte_color = 'r'
            self.ui.label_statut.setText(f"OUPS ! Hors de portée du robot Je ne peux réaliser que {self.nbPasARealiser} Pas")
            self.ui.statusbar.showMessage(f"OUPS ! Hors de portée du robot Je ne peux réaliser que {self.nbPasARealiser} Pas")
        else:
            self.nbPasARealiser = self.NbPas.value()
            porte_color = 'g'
            self.ui.statusbar.showMessage("A la portée du robot")

        if(self.nbPasARealiser == 0):
            self.ui.pushButton_mouvement.setEnabled(False)


        [Ax0 , Ay0 , _ , _] = self.A0
        Ax0 = round(float(Ax0) , 2)
        Ay0 = round(float(Ay0) , 2)
        
        [Art20x , Art20y , _ , _] = self.Art20
        [Art10x , Art10y ] = [self.L0.value() , 0]

        Y = np.array([0 , Art10y , Art20y ,  Ay0] , dtype="float")
        X = np.array([0 , Art10x , Art20x  , Ax0] , dtype="float")

        self.axes.clear()
        self.axes.invert_xaxis()
        self.axes.set_title('Robot manipulateur 2D')
        self.axes.set_xlabel('L\'axe des Y')
        self.axes.set_ylabel('L\'axe des X')
        self.line, = self.axes.plot(Y , X , linewidth=5)

        #dessine les articulations
        self.lineArt10, =  self.axes.plot(Art10y , Art10x , 'ko' , markersize =10)
        self.lineArt20, =self.axes.plot(Art20y , Art20x ,'ko' , markersize=10)
        
        #dessine la base 
        self.axes.plot([-0.25 , 0.25] , [0 , 0] , 'k' , linewidth=7)

        #dessine la trajectoire
        Y = np.array([Ay0 , self.Yb.value()] , dtype="float")
        X = np.array([Ax0 , self.Xb.value()] , dtype="float")
        
        self.axes.plot(Y , X , color=porte_color)
        self.axes.plot(self.Yb.value() , self.Xb.value() , marker='o' , color=porte_color)

        #légende sur le plot Hors de porté ét à sa porte

        self.axes.plot([] , [] , 'o' , color='r' , markersize=7 , label="HORS DE SA PORTEE")
        self.axes.plot([] , [] , 'o' , color='g', markersize=7 , label="A SA PORTEE")
        self.axes.legend()

        #dessine la pince
        self.linePince  = self.axes.scatter(Ay0,Ax0,marker='$\pitchfork$',s=400,color = "black",zorder=3)

        #défini le re^pêre
        mini=min(0,self.Yb.value(), self.A0[1])-1
        if mini==0:
            mini= -0.3

        self.axes.set_xlim(mini,max(0,self.Yb.value(),self.A0[1])+1)
        self.axes.set_ylim(min(self.Xb.value(),self.A0[0],0)-0.3,max(self.Xb.value(),self.A0[0],0,Art20x)+3) 
        
        self.axes.invert_xaxis()
        #ajoute des grilles
        self.axes.grid(True)

        #dessine le sol
        Y = np.array([0 , max(0 , self.Yb.value() , self.A0[1]) + 0.5] , dtype="float")
        X = np.array([0 ,0 ] , dtype="float")
        self.axes.plot( Y , X , ':k')

        #ecrire le mot sol
        self.axes.scatter(max(0 , self.Yb.value() , Ay0 ) + 0.7 , 0 , marker='$Sol$',s=400,color = "black",zorder=4)
        self.canvas.draw()

        #donnes d'ffichage position courante X Y teta1 et teta2
        self.ui.label_18.setText(str(round(self.A0[0] , 2)))
        self.ui.label_20.setText(str(round(self.A0[1] , 2)))
        self.ui.label_22.setText(str(self.teta1.value()))
        self.ui.label_24.setText(str(self.teta2.value()))



    def get_portee(self):
        W,  Z1, Z2 = self.L2.value(), 0 , - self.L1.value()
        for i in range (1 , self.NbPas.value() +1):
            Xk , Yk = self.getCoordonnes(i)
            X = Yk
            Y = self.L0.value() - Xk
            B1 = 2*(Y*Z1 + X*Z2)
            B2 = 2* (X*Z1 - Y*Z2)
            B3 = W**2 - X**2 - Y**2 -Z1*Z1 - Z2*Z2
            if B1**2 + B2**2 - B3**2 < 0 :
                return (False , i -1)
        return (True , i)

    def getPasX(self):
        return (self.Xb.value() - self.A0[0]) / self.NbPas.value()

    def getEquation(self):
        a = (self.A0[1] - self.Yb.value()) / (self.A0[0] - self.Xb.value())
        b = self.Yb.value() - a*self.Xb.value()
        return (a, b)

    def getCoordonnes(self , k):
        Xk = self.A0[0] + k* self.getPasX()
        a, b = self.equation
        Yk = a*Xk + b
        return [Xk , Yk]

    def Art12(self , k):
        """
        Déterminer teta1 et teta2 en fonction de k
        """
        Xk , Yk = self.getCoordonnes(k)

        W , X , Y , Z1 , Z2 = self.L2.value() , Yk ,  self.L0.value() - Xk ,  0 , -self.L1.value()
        B1 = 2 * (Y*Z1 + X*Z2)
        B2 = 2* (X*Z1 - Y*Z2)
        B3 = W**2 - X**2 - Y**2 -Z1*Z1 - Z2*Z2
        
        e = [1 , -1]
        SO1 = [0 , 0]
        CO1 = [0 , 0]
        tab_O1 = [0,0]
        O1 =0

        """Recherche de teta1 => O1"""
        if B3 !=0:
            for i in range(0, 2):
                SO1[i] = (B3 * B1 + e[i]*B2*math.sqrt(B1**2 + B2**2 - B3**2))/(B1**2 + B2**2)
                CO1[i] = (B3*B2 - e[i]*B1*math.sqrt(B1**2+ B2**2 - B3**2))/(B1**2+ B2**2)
                tab_O1[i] = math.degrees(math.atan2(SO1[i] , CO1[i]))

                if tab_O1[i] > 0:
                    O1 = round(tab_O1[i] , 2)
                    break
        else :
            O1 = round(math.degrees(math.atan2(-B2 , B1)) , 2)

        """ Recherche de teta2 => O2"""
        X1 = W
        X2 = W

        Y1 = X*math.cos(math.radians(O1)) + Y*math.sin(math.radians(O1)) + Z1
        Y2 = X*math.sin(math.radians(O1)) - Y*math.cos(math.radians(O1)) + Z2

        O2 = round(math.degrees(math.atan2(Y1/X1 , Y2/X2)) , 2)
        return (O1,O2)

            
    def mouvement(self):
        nbPas = self.NbPas.value()
        #coordonnes des Pk(Xk , Yk) pour les placer sur la droite
        coord = [self.getCoordonnes(i) for i in range(1 , nbPas + 1)]
        self.linePoint = [None] * nbPas
        for i in range(0 , nbPas):
            color ='g'
            if ( i >= self.nbPasARealiser):
                color = 'r'
            self.linePoint[i] = self.axes.scatter(coord[i][1] , coord[i][0] , marker='o', s=100 , color=color)
        self.annot = self.axes.annotate("", xy=(0,0), xytext=(20,20),textcoords="offset points",
                            bbox=dict(boxstyle="round", fc="w"),
                            arrowprops=dict(arrowstyle="->"))
        self.annot.set_visible(False)
        self.canvas.mpl_connect("motion_notify_event", self.hover)
        #animation de mouvement
        self.anim = FuncAnimation(self.ui.MplWidget , func=self.step_move , frames=np.arange(1 , self.nbPasARealiser +1) , interval=50 , repeat=False)
        self.canvas.draw()


    def step_move(self , k):   
        O1 , O2 = self.Art12(k)
        O1 = math.radians(O1)
        O2 = math.radians(O2)
        T10 = np.matrix([
            [ math.cos(O1) , -math.sin(O1) , 0 , self.L0.value()],
            [ math.sin(O1) , math.cos(O1) , 0 , 0],
            [0 , 0, 1 ,0],
            [0 , 0 ,0 ,1]
        ] )

        #matrice de passage de 2 à 1
        T21 = np.matrix([
            [math.cos(O2) , -math.sin(O2) , 0, self.L1.value()],
            [math.sin(O2) , math.cos(O2) , 0 , 0],
            [0, 0, 1, 0],
            [0, 0, 0, 1]
        ] )

        T20 = np.dot(T10 , T21)
        Art22 = np.array([
            [0],
            [0],
            [0],
            [1]
        ] , dtype=object)
        
        Art20 = np.dot(T20 , Art22)
        Art20 = np.squeeze(np.asarray(Art20))

        coord = self.getCoordonnes(k)

        X = np.array([0 , self.L0.value() , Art20[0] , coord[0]] , dtype="float")
        Y = np.array([0 , 0 , Art20[1] , coord[1]] , dtype="float")
        self.line.set_ydata(X)
        self.line.set_xdata(Y)


        # #dessine les articulations
        self.lineArt20.set_ydata(Art20[0])
        self.lineArt20.set_xdata(Art20[1])

        # #dessine la pince
        Ay0 = coord[1]
        Ax0 = coord[0]

        Ax0 = np.squeeze(np.asarray(Ax0))
        Ay0 = np.squeeze(np.asarray(Ay0))

        self.linePince.set_offsets(np.c_[Ay0 , Ax0])
        #donnes d'ffichage position courante X Y teta1 et teta2
        self.ui.label_18.setText(str(round(float(Ax0) , 2)))
        self.ui.label_20.setText(str(round(float(Ay0) , 2)))
        self.ui.label_22.setText(str(math.degrees(O1)))
        self.ui.label_24.setText(str(math.degrees(O2)))
        return self.line,

    def reprendre(self):
        self.axes.clear()
        self.L0.clear()
        self.L1.clear()
        self.L2.clear()
        self.teta1.clear()
        self.teta2.clear()
        self.Xb.clear()
        self.Yb.clear()
        self.NbPas.clear()

        self.canvas.draw()

    def quitter(self):
        self.hide()
        # sys.exit(self.app.exec_()) 

    def a_propos(self):
        pass

    def help(self):
        print("help")
        # form = Ui_Form()
        # form.exec_()



